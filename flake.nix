{
  description = "Silver Twilight - HTTP and file based DNS as last resort";

  inputs = {

    nixpkgs.url = "nixpkgs/nixpkgs-unstable";

    flake-utils.url = "github:numtide/flake-utils";

    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  nixConfig = {
    bash-prompt = ''\[\e[0;1;33m\]SilverTwilight\[\e[0;1m\]:\[\e[0;1;34m\]\w\[\e[0;1m\]$ \[\e[0m\]'';
  };

  outputs = { self, nixpkgs, flake-utils, ... }:
    let
      hsOvl = final: prev: {
        haskell = prev.haskell // {
          packageOverrides = hfinal: hprev: prev.haskell.packageOverrides hfinal hprev // {
            Spock-core = hfinal.callCabal2nix "Spock-core" ("${prev.fetchFromGitHub {
              owner = "agrafix";
              repo = "Spock";
              rev = "40d028bfea0e94ca7096c719cd024ca47a46e559";
              hash = "sha256-HIsVmGa9eOBjIc70asMuYbarv8C5ipxucAKUGltpbpc=";
            }}/Spock-core") { };

            SilverTwilight = hfinal.callCabal2nix "SilverTwilight" ./. { };
          };
        };
      };
    in
    flake-utils.lib.eachSystem [ "x86_64-linux" ]
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [ hsOvl ];
          };
        in
        {
          packages = {
            default = pkgs.haskellPackages.SilverTwilight;
            static = pkgs.pkgsStatic.haskellPackages.SilverTwilight;
          };

          devShells.default = with pkgs; mkShell {

            buildInputs =
              let
                hsEnv = haskellPackages.ghcWithPackages (_: with self.packages."${system}".default; buildInputs ++ propagatedBuildInputs);
              in
              [
                hsEnv
                ormolu
                haskell-language-server
                hpack
                cabal-install
                hlint
                nixpkgs-fmt
              ];
          };

          formatter = pkgs.nixpkgs-fmt;

        }) // {
      nixosModules.silvertwilight = import ./modules/nixos/silvertwilight.nix self.packages.x86_64-linux.default;

      overlays.default = hsOvl;
    };
}
