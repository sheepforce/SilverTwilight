# SilverTwilight
SilverTwilight is a HTTP and file based dynamic DNS solution written in Haskell for Linux.
It is meant to be used if a proper DNS solution cannot be setup and consists of two programmes.

**Carl** is the server component and runs on a host with name resolution or a static address.
**John** is the client component running on all machines that need name resolution.
Carl listens on a TCP port for HTTP post requests from the clients and collects all their address <-> name associations.
Whenever a change is received Carl will respond with updated host information to the client.
John periodically requests the IP address of a configured interface and the hostname of the machine it is running on.
Those information will be sent to Carl and Carl will respond with DNS information.
John then updates a `/etc/hosts` compatible file (usually `/etc/hosts`) with Carls information.
Consequently, all Johns will have name resolution between each other after a full cycle.

## Build
SilverTwilight is written in Haskell ad builds with Cabal.
Assuming GHC and Cabal are available:
```
cabal build
cabal install --prefix=$INSTALL_DIR
```
It is however more convenient to build using Nix:
```
nix profile install .
```

## Using
Carl runs stateless, does not access files and does not require special privileges.
Carl only requires an open TCP port, by default 38381.
Run `carl --help` for more information.

John needs write permissions to `/etc/hosts` and thus needs to be run as root.

`modules/opensuse/silvertwilight-*.service` contains example systemd service units for OpenSuse which should be easy to adapt for other systems.

The Flake provides `nixosModules.silvertwilight` for NixOS.