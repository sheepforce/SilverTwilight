{-# LANGUAGE LambdaCase #-}

module SilverTwilight.Common
  ( HasSecret (..),
    Parseable (..),
    convIP4,
    convIP6,
    HostName,
    unwrapHostName,
    printHostName,
    IpAddress (..),
    HostInfo (..),
    TimedHostInfo (..),
    preprocessHostsFile,
    mkHostEntry,
    mkHostEntryTimed,
  )
where

import Data.Aeson
import qualified Data.Aeson.KeyMap as KeyMap
import Data.Attoparsec.Text as Attoparsec
import Data.Textual as Textual
import Network.IP.Addr
import Network.Info
import RIO
import qualified RIO.Map as Map
import qualified RIO.Text as T
import RIO.Time

-- | Environments with access to the authorisation secret.
class HasSecret env where
  secretL :: Lens' env ByteString

-- | Stuff that can be parsed from a text value.
class Parseable a where
  parseT :: Parser a
  runParseT :: Text -> Either String a
  runParseT = parseOnly parseT

-- | Convert IP4 addresses from two different libraries.
convIP4 :: IPv4 -> IP4
convIP4 ipv4 = case Textual.fromString . show $ ipv4 of
  Just r -> r
  Nothing -> error "Cannot convert IPv4 address"

convIP6 :: IPv6 -> IP6
convIP6 ipv6 = case Textual.fromString . show $ ipv6 of
  Just r -> r
  Nothing -> error "Cannot convert IPv6 address"

-- | A hostname is a simple text (with some restrictions).
newtype HostName = HostName {unwrapHostName :: Text}
  deriving (Generic, Semigroup, Show, Eq)

instance FromJSON HostName

instance ToJSON HostName

instance Parseable HostName where
  parseT = HostName . T.pack <$> many1 (letter <|> digit <|> char '.' <|> char '-')

printHostName :: HostName -> Text
printHostName (HostName hn) = hn

-- | An IP address, either v4 or v6
data IpAddress
  = V4 IP4
  | V6 IP6
  deriving (Show, Eq, Ord)

instance Parseable IpAddress where
  parseT = do
    ipString <- many1 $ satisfy (`elem` (".:abcdef0123456789" :: String))
    let ipAddr =
          (V4 <$> Textual.fromString @IP4 ipString)
            <|> (V6 <$> Textual.fromString @IP6 ipString)
    case ipAddr of
      Nothing -> fail $ "Could not parse " <> ipString <> " as IP address"
      Just a -> return a

instance ToJSON IpAddress where
  toJSON (V4 ip4) = toJSON @Object . KeyMap.singleton "ip4" . toJSON . toText $ ip4
  toJSON (V6 ip6) = toJSON @Object . KeyMap.singleton "ip6" . toJSON . toText $ ip6

instance FromJSON IpAddress where
  parseJSON (Object v) = getIP4 <|> getIP6
    where
      getIP4 =
        fromText <$> v .: "ip4" >>= \case
          Nothing -> fail "Could not convert textual IPV4 representation"
          Just ip -> return . V4 $ ip
      getIP6 =
        fromText <$> v .: "ip6" >>= \case
          Nothing -> fail "Could not convert textual IPV4 representation"
          Just ip -> return . V6 $ ip
  parseJSON e = fail $ "Could not parse " <> show e <> " as IpAddress"

instance ToJSONKey IpAddress

instance FromJSONKey IpAddress

-- | Host info is the mapping from a IPv4 address to a hostname and a
-- potentially empty list of aliases.
newtype HostInfo = HostInfo (Map IpAddress (HostName, [HostName]))
  deriving (Generic, Show)

instance FromJSON HostInfo

instance ToJSON HostInfo

instance Parseable HostInfo where
  parseT = do
    hostInfo <- many' hostParser
    endOfInput
    return . HostInfo . Map.fromList $ hostInfo
    where
      hostParser :: Parser (IpAddress, (HostName, [HostName]))
      hostParser = do
        ipAddr <- parseT @IpAddress
        skipSpace
        hn <- hostNameParser
        aliases <- many' $ skipHSpace *> hostNameParser
        endOfLine
        return (ipAddr, (hn, aliases))

      hostNameParser :: Parser HostName
      hostNameParser =
        fmap (HostName . T.pack)
          . many1
          $ letter <|> digit <|> char '.' <|> char '-'

      skipHSpace :: Parser ()
      skipHSpace = skip (\c -> c `elem` [' ', '\t', '\f', '\v'])

-- | 'HostInfo' but annotated with the time the last update from a particular
-- host was received.
newtype TimedHostInfo = TimedHostInfo (Map IpAddress (UTCTime, HostName, [HostName]))
  deriving (Generic, Show)

instance FromJSON TimedHostInfo

instance ToJSON TimedHostInfo

-- | Preprocess an @/etc/hosts@ file and remove all comment lines and blank
-- lines.
preprocessHostsFile :: Text -> Text
preprocessHostsFile = T.unlines . filter filterF . filter (/= "") . T.lines
  where
    filterF :: Text -> Bool
    filterF t = T.take 1 t `notElem` [" ", "\n", "#"]

-- | Generate a @/etc/hosts@ compatible file.
mkHostEntry :: HostInfo -> Text
mkHostEntry (HostInfo hi) = T.unlines . fmap printF . Map.toList $ hi
  where
    printF :: (IpAddress, (HostName, [HostName])) -> Text
    printF (ip, (HostName fqdn, aliases)) =
      T.unwords $
        printIpF ip : fqdn : fmap (\(HostName alias) -> alias) aliases

    printIpF :: IpAddress -> Text
    printIpF (V4 a) = toText a
    printIpF (V6 a) = toText a

-- | Generate a @/etc/hosts@ compatible file.
mkHostEntryTimed :: TimedHostInfo -> Text
mkHostEntryTimed (TimedHostInfo hi) = T.unlines . fmap printF . Map.toList $ hi
  where
    printF :: (IpAddress, (UTCTime, HostName, [HostName])) -> Text
    printF (ip, (time, HostName fqdn, aliases)) =
      T.unwords $
        printIpF ip
          : fqdn
          : fmap (\(HostName alias) -> alias) aliases
            <> pure "# last updated at "
            <> (pure . T.pack $ formatTime defaultTimeLocale "%d.%m.%y - %T" time)

    printIpF :: IpAddress -> Text
    printIpF (V4 a) = toText a
    printIpF (V6 a) = toText a