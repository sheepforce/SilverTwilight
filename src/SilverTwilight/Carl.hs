module SilverTwilight.Carl where

import Network.HTTP.Types.Status
import RIO hiding (timeout)
import qualified RIO.ByteString as BS
import qualified RIO.Map as Map
import RIO.Time
import SilverTwilight.Common
import System.Console.CmdArgs
import Web.Spock
import Web.Spock.Config (PoolOrConn (PCNoDatabase), defaultSpockCfg)

class HasForgetTime env where
  forgetTimeL :: Lens' env DiffTime

class HasUpdateSinkPort env where
  updateSinkPortL :: Lens' env Word16

-- | Runtime environment of Carl.
data CarlEnv = CarlEnv
  { logFunc :: LogFunc
  , forgetTime :: DiffTime
  -- ^ The time after which to forget a host if it hasn't sent updates.
  , updateSinkPort :: Word16
  -- ^ The TCP port the web API is running at.
  , secret :: ByteString
  -- ^ A secret key that needs to be part of the request to accept the request.
  }

instance HasLogFunc CarlEnv where
  logFuncL = lens logFunc $ \env b -> env{logFunc = b}

instance HasForgetTime CarlEnv where
  forgetTimeL = lens forgetTime $ \env b -> env{forgetTime = b}

instance HasUpdateSinkPort CarlEnv where
  updateSinkPortL = lens updateSinkPort $ \env b -> env{updateSinkPort = b}

instance HasSecret CarlEnv where
  secretL = lens (\CarlEnv{secret} -> secret) $ \env b -> (env{secret = b} :: CarlEnv)

-- | Spock server state - stores the 'TimedHostInfo'.
newtype HostsState = HostsState (TVar TimedHostInfo)

-- | The Spock session - No users are treated, therefore empty.
data NoUserSession = NoUserSession

{- | Update 'TimedHostInfo' with newly received 'HostInfo' and the current time.
 Removes too old entries.
-}
updateTimedHostInfo ::
  (HasForgetTime env, HasLogFunc env) =>
  HostInfo ->
  ReaderT env (ActionCtxT () (WebStateM () NoUserSession HostsState)) ()
updateTimedHostInfo hostInfoUpdate = do
  -- Get environment
  forgetTime <- view forgetTimeL
  currTime <- getCurrentTime
  HostsState hostsStateT <- lift getState

  -- Compare old and new hostinformation. Remove hosts that are too old.
  (oldInfo, newInfo) <- atomically $ do
    TimedHostInfo oldTimedHostInfo <- readTVar hostsStateT
    let cleanedTimedHostInfo =
          Map.filter
            (\(t, _, _) -> filterOldTime forgetTime currTime t)
            oldTimedHostInfo
        TimedHostInfo timedHostInfoUpdate = hostInfoToTimedHostInfo currTime hostInfoUpdate
        newHostInfo = timedHostInfoUpdate `Map.union` cleanedTimedHostInfo
    writeTVar hostsStateT . TimedHostInfo $ newHostInfo
    return (oldTimedHostInfo, newHostInfo)

  -- Compare old and new hosts and inform about host that were removed
  let remHosts = oldInfo `Map.difference` newInfo
  logInfo $ "Current host info:\n" <> (display . mkHostEntryTimed . TimedHostInfo $ newInfo)
  unless (remHosts == mempty)
    . logWarn
    $ "Removed old hosts due to age: " <> displayShow remHosts
 where
  filterOldTime :: DiffTime -> UTCTime -> UTCTime -> Bool
  filterOldTime dt currT oldT =
    let oldNewDiff = toRational $ diffUTCTime currT oldT
     in oldNewDiff < toRational dt

  hostInfoToTimedHostInfo :: UTCTime -> HostInfo -> TimedHostInfo
  hostInfoToTimedHostInfo currT (HostInfo hi) =
    TimedHostInfo
      . fmap (\(hn, aliases) -> (currT, hn, aliases))
      $ hi

-- | Respond to a request with the updated and collected host information.
respondWithHosts :: ReaderT env (ActionCtxT () (WebStateM () NoUserSession HostsState)) ()
respondWithHosts = do
  HostsState hostsStateT <- lift getState
  TimedHostInfo hi <- readTVarIO hostsStateT
  let hostInfo = fmap (\(_, hn, alias) -> (hn, alias)) hi
  lift . json . HostInfo $ hostInfo

{- | Sink that receives 'HostInfo' updates from John and updates 'TimedHostInfo'
 in 'HostState' accordingly.
-}
updateSink ::
  (HasLogFunc env, HasForgetTime env, HasSecret env) =>
  env ->
  SpockM () NoUserSession HostsState ()
updateSink env = do
  secret <- flip runReaderT env $ view secretL
  post "hosts" . flip runReaderT env $ do
    passwd <- lift $ rawHeader "Authorization"
    case passwd of
      Nothing -> do
        logError "Header did not provide Authorization, unauthorised!"
        lift $ setStatus unauthorized401
      Just s
        | s /= secret -> do
            logError "Wrong secret provided"
            lift $ setStatus unauthorized401
        | otherwise -> do
            bdy <- lift jsonBody
            case bdy of
              Nothing -> do
                logError "POST: Could not parse host info JSON from John"
                lift $ setStatus badRequest400
              Just hostinfo -> do
                logInfo $
                  "Received host information:\n"
                    <> (display . mkHostEntry $ hostinfo)
                lift $ setStatus ok200
                updateTimedHostInfo hostinfo
                respondWithHosts

{- | The client program Carl. Listens to updates from many Johns and collects
 their host information to build a DNS. Whenever an update is received let all
 Johns know about the new information.
-}
server ::
  ( HasLogFunc env
  , HasForgetTime env
  , HasUpdateSinkPort env
  , HasSecret env
  ) =>
  RIO env ()
server = do
  hostsState <- HostsState <$> (newTVarIO . TimedHostInfo $ mempty)
  listenToJohn hostsState
 where
  listenToJohn hostState = do
    env <- ask
    spockPort <- view updateSinkPortL
    liftIO $ do
      spockCfg <- defaultSpockCfg NoUserSession PCNoDatabase hostState
      runSpock (fromIntegral spockPort) . spock spockCfg . updateSink $ env

-- | Command line arguments to Carl, used to initialise the 'CarlEnv'
data CarlArgs = CarlArgs
  { timeout :: Word
  , portC :: Word16
  , secret :: Maybe FilePath
  }
  deriving (Show, Data, Typeable)

carl :: CarlArgs
carl =
  CarlArgs
    { timeout =
        600
          &= help "Timeout in seconds after which a host that didn't send an update is forgotten"
          &= typ "seconds"
    , portC =
        38381
          &= help "TCP port that Carl listens on for updates from John"
          &= typ "tcp port"
          &= explicit
          &= name "port-carl"
    , secret =
        Nothing
          &= help "Path to a file containing the authentication secret"
          &= typFile
    }
    &= help "Receiving and distributing DNS information from a swarm of hosts to those hosts - aka HTTP and file based dynamic DNS"
    &= summary "Carl - Ph'nglui mglw'nafh - Central DNS management program"

carlMain :: IO ()
carlMain = do
  -- Get command line arguments
  CarlArgs{..} <- cmdArgs carl
  secretCnt <- case secret of
    Just p -> BS.filter (/= 10) <$> readFileBinary p
    Nothing -> throwString "No secret file provided, can not continue"

  -- Setup logging and environment
  logOptions' <- logOptionsHandle stderr False
  let logOptions = setLogVerboseFormat True . setLogUseTime True $ logOptions'
  withLogFunc logOptions $ \lf -> do
    let carlEnv =
          CarlEnv
            { logFunc = lf
            , forgetTime = secondsToDiffTime . fromIntegral $ timeout
            , updateSinkPort = portC
            , secret = secretCnt
            }

    -- Run Carl
    runRIO carlEnv server
