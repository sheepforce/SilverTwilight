{-# LANGUAGE LambdaCase #-}

module SilverTwilight.John where

import Data.Aeson
import Network.HTTP.Simple
import qualified Network.HostName as HN
import Network.IP.Addr
import Network.Info as NI
import RIO
import qualified RIO.ByteString as BS
import qualified RIO.Map as Map
import qualified RIO.Text as T
import RIO.Time
import SilverTwilight.Common
import System.Console.CmdArgs as CA

-- | The name of a network interface, e.g. eno1.
type InterfaceName = String

-- | Access to the web interface of Carl
class HasServerSettings env where
  serverSettingsL :: Lens' env ServerSettings

-- | Info about the interface name to query
class HasInterfaceName env where
  interfaceNameL :: Lens' env InterfaceName

class HasUpdateInterval env where
  updateIntervalL :: Lens' env DiffTime

class HasTemplateHosts env where
  templateHostsL :: Lens' env HostInfo

class HasHostsFile env where
  hostsFileL :: Lens' env FilePath

-- | Runtime environment of the client app.
data JohnEnv = JohnEnv
  { logFunc :: LogFunc
  , serverSettings :: ServerSettings
  -- ^ Connection settings how to reach the web API of Carl.
  , interfaceName :: InterfaceName
  -- ^ The name of the network interface on the local machine to monitor, e.g.
  -- @eno1@.
  , updateInterval :: DiffTime
  -- ^ How often to query the host's IP address on the interface.
  , templateHosts :: HostInfo
  -- ^ A constant 'HostInfo' that will always be combined with the one
  -- received from Carl.
  , hostsFile :: FilePath
  -- ^ The file that is to be updated, usually @/etc/hosts@.
  , secret :: ByteString
  -- ^ The secret for authorisation at Carl.
  }

instance HasLogFunc JohnEnv where
  logFuncL = lens logFunc $ \env b -> env{logFunc = b}

instance HasServerSettings JohnEnv where
  serverSettingsL = lens serverSettings $ \env b -> env{serverSettings = b}

instance HasInterfaceName JohnEnv where
  interfaceNameL = lens interfaceName $ \env b -> env{interfaceName = b}

instance HasUpdateInterval JohnEnv where
  updateIntervalL = lens updateInterval $ \env b -> env{updateInterval = b}

instance HasTemplateHosts JohnEnv where
  templateHostsL = lens templateHosts $ \env b -> env{templateHosts = b}

instance HasHostsFile JohnEnv where
  hostsFileL = lens hostsFile $ \env b -> env{hostsFile = b}

instance HasSecret JohnEnv where
  secretL = lens secret $ \env b -> env{secret = b}

-- | Carl's web API connection settings.
data ServerSettings = ServerSettings
  { host :: ByteString
  -- ^ The FQDN to use for a HTTP POST request.
  , port :: Word16
  -- ^ The port at which Carl's web API is running.
  }
  deriving (Generic, Show)

-- | Get the 'HostName' of this machine.
getHostName :: (HasLogFunc env) => RIO env HostName
getHostName = do
  hn' <- liftIO HN.getHostName
  case runParseT @HostName . T.pack $ hn' of
    Left _err -> do
      logError $ "Could not parse " <> displayShow hn' <> " as hostname"
      throwString "Could not parse hostname"
    Right hn -> do
      logInfo $ "Got hostname " <> (displayShow . unwrapHostName $ hn)
      return hn

-- | Get the IPv4 address of the main interface.
getIPv4Addr :: (HasLogFunc env, HasInterfaceName env) => RIO env (IP4, IP6)
getIPv4Addr = do
  ifcN <- view interfaceNameL
  logDebug $ "main network interface: " <> displayShow ifcN

  ifcs <- liftIO getNetworkInterfaces
  case filter (\ifc -> NI.name ifc == ifcN) ifcs of
    [NetworkInterface{ipv4, ipv6}] -> do
      let ip4 = convIP4 ipv4
          ip6 = convIP6 ipv6
      logInfo $ "interface IP4: " <> displayShow ipv4 <> "\ninterface IP6: " <> displayShow ip6
      return (ip4, ip6)
    _ -> logError "could not find IPv4 addr for interface" >> throwString ""

{- | Make a HTTP POST request to the server with information about the local
 machine. The server responds with the collected hostinfo.
-}
exchangeHostInfo ::
  (HasServerSettings env, HasLogFunc env, HasSecret env) =>
  HostName ->
  (IP4, IP6) ->
  RIO env HostInfo
exchangeHostInfo hn (ip4, ip6) = do
  ServerSettings{..} <- view serverSettingsL
  secret <- view secretL
  let req =
        addRequestHeader "Authorization" secret
          . setRequestHost host
          . setRequestPort (fromIntegral port)
          $ defReq
  logInfo $
    "POST update to Carl:\n"
      <> (displayBytesUtf8 . toStrictBytes . encode $ hostInfo)

  eresp <- try $ httpJSON req
  case eresp of
    Left (err :: SomeException) -> do
      logError $ "Exception during POST request " <> displayShow err
      throwString $ "Could not POST the hostinfo to Carl with: " <> show err
    Right resp
      | getResponseStatusCode resp `notElem` [200, 201] -> do
          logError $
            "POST update not successfull, got status"
              <> display (getResponseStatusCode resp)
          throwString "Could not understand hostinfo response from Carl"
      | otherwise -> do
          logDebug "Successfully POSTed update"
          let dnsHostInfo = getResponseBody resp
          logInfo $
            "Got HostInfo from Carl:\n"
              <> display (mkHostEntry dnsHostInfo)
          return dnsHostInfo
 where
  hostInfo =
    HostInfo . Map.fromList $
      [ (V4 ip4, (hn, mempty))
      , (V6 ip6, (hn, mempty))
      ]

  defReq =
    setRequestCheckStatus
      . setRequestSecure True
      . setRequestPath "/hosts"
      . setRequestBodyJSON hostInfo
      . setRequestMethod "POST"
      $ defaultRequest

{- | The client programme John. Periodically sends information about its host to
 Carl who responds with the collected DNS information. John then writes the
 DNS information to a file.
-}
client ::
  ( HasLogFunc env
  , HasServerSettings env
  , HasUpdateInterval env
  , HasTemplateHosts env
  , HasInterfaceName env
  , HasHostsFile env
  , HasSecret env
  ) =>
  RIO env ()
client = do
  HostInfo tmplHosts <- view templateHostsL
  void . updateLoop . HostInfo $ tmplHosts
 where
  updateLoop (HostInfo tmplHosts) = forever $ do
    hn <- getHostName
    ip <- getIPv4Addr
    HostInfo hostInfo <- catch (exchangeHostInfo hn ip) (catchExchErr $ HostInfo tmplHosts)
    hostsFilePath <- view hostsFileL
    let hosts = Map.unionWith (<>) tmplHosts hostInfo
        hostsFile = mkHostEntry . HostInfo $ hosts
    writeFileUtf8 hostsFilePath hostsFile
    delay

  catchExchErr tmplHosts err = do
    logError $ "HostInfo exchange with Carl went wrong: " <> displayShow (err :: StringException)
    delay
    updateLoop tmplHosts

  delay = do
    updIntvl <- view updateIntervalL
    let microSeconds = fromInteger . (`div` 1000000) . diffTimeToPicoseconds $ updIntvl
    threadDelay microSeconds

-- | Command line arguments to John, used to initialise the 'JohnEnv'
data JohnArgs = JohnArgs
  { carlHostName :: String
  , carlPort :: Word16
  , interface :: String
  , interval :: Word
  , template :: FilePath
  , target :: FilePath
  , secretFile :: Maybe FilePath
  }
  deriving (Show, Data, Typeable)

john :: JohnArgs
john =
  JohnArgs
    { carlHostName =
        mempty
          &= help "Address or hostname at which Carl listens"
          &= typ "hostname"
          &= explicit
          &= CA.name "carl-address"
    , carlPort =
        38381
          &= help "Port at which Carl listens for updates"
          &= typ "port"
          &= explicit
          &= CA.name "carl-port"
    , interface =
        "eno1"
          &= help "name of the network interface to update, e.g. eno1"
          &= typ "network interface name"
    , interval =
        300
          &= help "Time in seconds between updates of the address associated with the interface"
          &= typ "seconds"
    , template =
        "/etc/hosts.bak"
          &= help "Original hosts file that should be merged into Carl's info"
          &= typFile
    , target =
        "/etc/hosts"
          &= help "Dynamic hosts file, commonly /etc/hosts"
          &= typFile
    , secretFile =
        Nothing
          &= help "Path to a secret used for authorisation against Carl"
          &= typFile
          &= explicit
          &= CA.name "secret"
    }
    &= help "Receiving and distributing DNS information from a swarm of hosts to those hosts - aka HTTP and file based dynamic DNS"
    &= summary "John - Cthulhu Fhtagn - Distributed DNS update program"

johnMain :: IO ()
johnMain = do
  -- Get command line arguments
  JohnArgs{..} <- cmdArgs john
  secretCnt <- case secretFile of
    Just p -> BS.filter (/= 10) <$> readFileBinary p
    Nothing -> throwString "No secret file specified, can not continue"

  -- Get the template host file
  originalHostsFile <- readFileUtf8 template
  templateHosts <- case runParseT . preprocessHostsFile $ originalHostsFile of
    Right res -> return res
    Left _err -> throwString "Could not parse template hosts file"

  -- Setup logging and environment
  logOptions' <- logOptionsHandle stderr False
  let logOptions = setLogVerboseFormat True . setLogUseTime True $ logOptions'
  withLogFunc logOptions $ \lf -> do
    let johnEnv =
          JohnEnv
            { logFunc = lf
            , serverSettings =
                ServerSettings
                  { host = fromString carlHostName
                  , port = carlPort
                  }
            , interfaceName = interface
            , updateInterval = secondsToDiffTime . fromIntegral $ interval
            , templateHosts
            , hostsFile = target
            , secret = secretCnt
            }

    -- Run Carl
    runRIO johnEnv client
