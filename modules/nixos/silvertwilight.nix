silvertwilight: { config, lib, pkgs, ... }:

with lib;

let cfg = config.services.silvertwilight;

in {
  options = {
    services.silvertwilight = {
      carl = {
        enable = mkOption {
          type = types.bool;
          default = false;
          description = mdDoc ''Server component of a file-based dynamic DNS service'';
        };

        portCarl = mkOption {
          type = types.int;
          default = 38381;
          description = mdDoc ''TCP port at which Carl listens to DNS updates from Clients'';
        };

        timeout = mkOption {
          type = types.int;
          default = 600;
          description = mdDoc ''Timeout in seconds after which a host that hasn't sent updates is forgotten'';
        };

        secret = mkOption {
          type = types.path;
          default = "/run/silvertwilight/secret";
          description = mdDoc ''Path to a secret key file used for authentification'';
        };
      };

      john = {
        enable = mkOption {
          type = types.bool;
          default = false;
          description = "Client component of a file-based dynamic DNS service";
        };

        portCarl = mkOption {
          type = types.int;
          default = 38381;
          description = "TCP port at which Carl listens to DNS updates from Clients";
        };

        addressCarl = mkOption {
          type = types.str;
          default = "127.0.0.1";
          description = "URL or address where the server component Carl is running";
        };

        interval = mkOption {
          type = types.int;
          default = 300;
          description = "Update interval in seconds";
        };

        interface = mkOption {
          type = types.str;
          default = "eno1";
          description = "Name of the network interface that is queried for IP address";
        };

        secret = mkOption {
          type = types.path;
          default = "/run/silvertwilight/secret";
          description = mdDoc ''Path to a secret key file used for authentification'';
        };

        /*
        template = mkOption {
          type = types.path;
          default = "/etc/static/hosts";
          description = "Template hosts file thats information will always be merged into DNS information";
        };

        target = mkOption {
          type = types.path;
          default = "/etc/hosts";
          description = "Target file to be updated dynamically with DNS information from Carl";
        };
        */
      };
    };
  };

  config = {
    networking.firewall.allowedTCPPorts = optional cfg.carl.enable cfg.carl.portCarl;

    systemd.services = {
      "silvertwilight-carl" = {
        description = "SilverTwilight Carl - HTTP/file-based dynamic DNS server";
        enable = cfg.carl.enable;
        wantedBy = [ "multi-user.target" ];
        after = [ "network.target" ];
        path = [ silvertwilight ];

        script = ''
          carl \
            --port-carl=${builtins.toString cfg.carl.portCarl} \
            --timeout=${builtins.toString cfg.carl.timeout} \
            --secret=${cfg.carl.secret}
        '';

        serviceConfig = {
          User = "root";
          Group = "root";
        };
      };

      "silvertwilight-john" = {
        description = "SilverTwilight John - HTTP/file-based dynamic DNS client";
        enable = cfg.john.enable;
        wantedBy = [ "multi-user.target" ];
        after = [ "network.target" ];
        path = [ silvertwilight ];
        serviceConfig = {
          Restart = "on-failure";
          RestartSec = "15s";
        };

        /*
        preStart = ''
          if [[ -f "/etc/hosts" ]]; then
            if [[ -L "/etc/hosts" ]]; then
              unlink /etc/hosts
            else
              rm /etc/hosts
            fi
          fi          
        '';
        */
        script = ''
          if [[ -f "/etc/hosts" ]]; then
            if [[ -L "/etc/hosts" ]]; then
              unlink /etc/hosts
            else
              rm /etc/hosts
            fi
          fi          
          john \
            --carl-address=${cfg.john.addressCarl} \
            --carl-port=${builtins.toString cfg.john.portCarl} \
            --interface=${builtins.toString cfg.john.interface} \
            --template=/etc/static/hosts \
            --target=/etc/hosts \
            --interval=${builtins.toString cfg.john.interval} \
            --secret=${cfg.john.secret}
        '';
        postStop = "ln -s /etc/static/hosts /etc/hosts";

        serviceConfig = {
          User = "root";
          Group = "root";
          StandardOutput = "journal";
          StandardError = "journal";
        };
      };
    };
  };
}
